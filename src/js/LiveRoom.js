import { reactive } from 'vue'
const anchors = reactive({
    anchors: [
        {
            name: '小涛',
            anchorNo: 'xiaotao',
            identity: [{ title: '原神大佬', type: 'danger' }, { title: '退伍全栈JAVA研发工程架构师硕士', type: 'success' }, { title: 'Web前端开发工程师', type: '' }, { title: '摆烂交', type: 'info' }]
        },
        {
            name: '小熙',
            anchorNo: 'xiaoxi',
            identity: [{ title: '王者在线直播', type: 'success' }]
        },
        {
            name: '小新',
            anchorNo: 'xiaoxin',
            identity: [{ title: 'java工程硕士', type: 'warning' }]
        },
        {
            name: '懒惰的ZW',
            anchorNo: 'lazyzw',
            identity: [{ title: 'SpringCloud架构师', type: 'danger' }]
        },
        {
            name: '君权天下',
            anchorNo: 'junquan',
            identity: [{ title: '算法工程师', type: 'primary' }]
        },
        {
            name: '潜渣',
            anchorNo: 'qianzha',
            identity: [{ title: '反应式编程奠基人', type: 'success' }, { title: '蓝桥杯国家一等奖', type: 'danger' }]
        }
    ]
})

export { anchors }
