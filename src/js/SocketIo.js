/*
 * @Ahthor: xiaoxi
 */
// 建立socket.io通信
state.socket = io.connect('http://192.168.2.112:3001')
// 监听当前直播间的消息
state.socket.on(`chat-${props.name}`, (message) => {
    console.log(message)
    // 弹幕内容, 弹幕样式, 回调函数
    state.danmaku.send(message.danmakuContent, 'danmaku-wrapper')
    state.danmakuData.push(message.danmakuContent)
})
