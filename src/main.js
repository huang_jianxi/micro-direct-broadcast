import { createApp } from 'vue'
import App from './App.vue'
import router from './router'
import { store } from './store'
import './styles/index.scss'
import element3 from 'element3'
import 'element3/lib/theme-chalk/index.css'
const app = createApp(App)
app.use(router)
app.use(store)
app.use(element3)
app.mount('#app')
