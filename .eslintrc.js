module.exports = {
    root: true,
    env: {
        node: true
    },
    extends: ['plugin:vue/vue3-essential', 'eslint:recommended', 'standard'],
    parserOptions: {
        parser: 'babel-eslint'
    },
    globals: {
        defineProps: 'readonly',
        defineEmits: 'readonly'
    },
    rules: {
        'no-console': process.env.NODE_ENV === 'production' ? 'warn' : 'off',
        'no-debugger': process.env.NODE_ENV === 'production' ? 'warn' : 'off',
        camelcase: 0,
        'no-callback-literal': 0,
        indent: [
            2,
            4
        ],
        eqeqeq: 'off',
        'space-before-function-paren': [
            2,
            'never'
        ],
        'promise/param-names': 'off',
        'no-multiple-empty-lines': [
            2,
            {
                max: 5,
                maxBOF: 5
            }
        ]
    }
}
